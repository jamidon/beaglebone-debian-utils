#!/bin/bash -e
# Unpublished work © 2016, The Narwhal Group
# All Rights Reserved
#
#    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF THE NARWHAL GROUP
#    The copyright notice above does not evidence any
#    actual or intended publication of such source code.

# the fixed IP address of the unit
IP_ADDR=192.168.0.1
IP_GTWY=192.168.0.1
IP_MASK=255.255.255.0

ETH0=/sys/class/net/eth0/address

if [ ! -f $ETH0 ]
then
    echo "FAILED: $ETH0 not found"
    exit 1
fi

PATH=$PATH:/usr/sbin
CONNMANCTL=`which connmanctl`

if [ ! $CONNMANCTL ]
then
    echo "FAILED: connmanctl not found, maybe run as root?"
    exit 2
fi

MAC_ADDR=`cat $ETH0`
SERVICE=ethernet_${MAC_ADDR//:/}_cable

factory_reset() {
    echo "Setting to factory IP of 192.168.0.1"

    ${CONNMANCTL} config ${SERVICE} --ipv4 manual ${IP_ADDR} ${IP_MASK} ${IP_GTWY}
    rc=$?
    if [ $rc != "0" ];
    then
        exit $rc
    fi
    ${CONNMANCTL} config ${SERVICE} --ipv6 off
}

dhcp_reset() {
    echo "Setting interface to DHCP"

    ${CONNMANCTL} config ${SERVICE} --ipv4 dhcp
    rc=$?
    if [ $rc != "0" ];
    then
        exit $rc
    fi
}

# assume reset
RESET="Y"

# getopt stuff
SHORT=rdh
LONG=dhcp,reset,help
PARSED=`getopt --options $SHORT --longoptions $LONG --name "$0" -- "$@"`

# set the options, will at least have --
eval set -- $PARSED

while true; do
    case "$1" in
        -d|--dhcp)
            RESET="N"
            shift
            break
            ;;
        -r|--reset)
            RESET="Y"
            shift
            break
            ;;
        -h|--help)
            cat << EOF
Configure eth0 to DHCP or fixed IP address, usage:

    $0 [--dhcp|--reset]

If no options, assumes reset to fixed IP ${IP_ADDR}
EOF
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Unknown option"
            exit 3
            ;;
    esac
done

if [ ${RESET} = "Y" ]; then
    factory_reset
else
    dhcp_reset
fi
