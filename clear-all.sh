#!/bin/bash -e
# Unpublished work © 2017, The Narwhal Group
# All Rights Reserved
#
#    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF THE NARWHAL GROUP
#    The copyright notice above does not evidence any
#    actual or intended publication of such source code.

if [ $(id -u) != "0" ]; then
    echo "Must run this script as root!"
    exit 1
fi

( cd /var/log/ && rm -f auth.log.* daemon.log.* debug.* faillog.* kern.log.* lastlog.* messages.* syslog.* )
rm -f /var/local/narwhal.firstboot.done 
dd if=/dev/null of=/home/helix/.bash_history
dd if=/dev/null of=/var/log/auth.log
dd if=/dev/null of=/var/log/daemon.log
dd if=/dev/null of=/var/log/debug
dd if=/dev/null of=/var/log/faillog
dd if=/dev/null of=/var/log/kern.log
dd if=/dev/null of=/var/log/lastlog
dd if=/dev/null of=/var/log/messages
dd if=/dev/null of=/var/log/syslog
dd if=/dev/null of=/var/log/wtmp
